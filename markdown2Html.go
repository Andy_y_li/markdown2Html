package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/go-martini/martini"
	"github.com/russross/blackfriday"
)

func main() {
	fmt.Println("vim-go")
	m := martini.Classic()

	m.Get("/", index)

	m.RunOnAddr(":8080")
}

func index(w http.ResponseWriter, r *http.Request) string {
	input := readFile("./vim_README.md")
	output := blackfriday.MarkdownCommon([]byte(input))

	return string(output)
}

func readFile(path string) string {
	fi, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer fi.Close()
	fd, err := ioutil.ReadAll(fi)
	// fmt.Println(string(fd))
	return string(fd)
}
